from .extensions import db
from .models import Course
from .models import Student


def add_data():
    db.session.add_all(
        [
            Course(name="Math"),
            Course(name="Science"),
            Course(name="History"),
            Course(name="Reading"),
        ]
    )
    db.session.add_all(
        [
            Student(name="Anthony", course_id=1),
            Student(name="Britney", course_id=1),
            Student(name="Charlie", course_id=2),
            Student(name="Desiree", course_id=4),
        ]
    )
    db.session.commit()
