import os

from flask import Flask
from flask_migrate import Migrate

from .extensions import api
from .extensions import db
from .extensions import jwt
from .models import User
from .resources import ns

basedir = os.path.abspath(os.path.dirname(__file__))


def create_app():
    app = Flask(__name__)

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(basedir, "data.sqlite")
    app.config["JWT_SECRET_KEY"] = os.getenv("SECRET_KEY", default="BAD_SECRET_KEY")

    api.init_app(app)
    db.init_app(app)
    jwt.init_app(app)

    migrate = Migrate(app, db)
    api.add_namespace(ns)

    @jwt.user_identity_loader
    def user_identity_lookup(user):
        """
        Cette fonction est utilisée pour spécifier comment Flask-JWT-Extended doit extraire l'identité de l'utilisateur
        à partir des données de l'utilisateur. Lorsqu'un token JWT est décodé,
        Flask-JWT-Extended doit déterminer l'identité de l'utilisateur associée à ce token.
        Dans votre cas, cette fonction indique que l'identité de l'utilisateur est l'attribut "id" de l'objet User
        """
        return user.id

    @jwt.user_lookup_loader
    def user_lookup_callback(_jwt_header, jwt_data):
        """
        Cette fonction est utilisée pour spécifier comment Flask-JWT-Extended doit rechercher l'utilisateur associé
        """
        identity = jwt_data["sub"]
        return User.query.filter_by(id=identity).first()

    return app
